const express = require('express');
const app = express();

// creating middleware 
const requestTime = function (req, res, next) {
  console.log( 'Testing: ', Date());
  next();
}

//using the middleware created.
app.use(requestTime); // one

app.get('/', function (req, res) {
  let responseText = 'Hello World!';
  responseText += `<p>Requested at:  ${Date()}  </small>`;
  res.send(responseText);
});

app.use(requestTime); // two

app.get('/prdxn', function (req, res) {
  let responseText = 'Hello World!';
  responseText += `<p>Requested at:  ${Date()}  </small>`;
  res.send(responseText);
});

app.get('/prdxn/prdxn', (req, res, next) =>{
  console.log('Route specific middelware triggred.');
  next();
}, function (req, res) {
  let responseText = 'Hello World!';
  responseText += `<p>Requested at:  ${Date()}  </small>`;
  res.send(responseText);
});

app.listen(3000, () => console.log(`Example app listening on port 3000!`))